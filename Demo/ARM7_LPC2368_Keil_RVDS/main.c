
/* Standard includes. */
#include <stdlib.h>
// pour le lpc2378 Scheduler includes. 
#include "FreeRTOS.h"
#include "task.h"

#include "myTasks.h"

/* Priorities for the demo application tasks. */
#define mainLED_TASK_PRIORITY		( tskIDLE_PRIORITY + 2 )


 // Configuration minimal du processeur pour avoir les 8 Leds du Port2 disponible
 
static void prvSetupHardware( void );

/*-----------------------------------------------------------*/
void gestion_abort(void)
{


}


/*
 * Application entry point:
 * Starts all the other tasks, then starts the scheduler. 
 */
int main( void )
{
 // Configuration minimal du processeur pour avoir les 8 Leds du Port2 disponible
	prvSetupHardware();

	/* initialisation des taches */
	vInit_myTasks( mainLED_TASK_PRIORITY );

	// toutes les taches ont �t� d�marr�es - Demarrer le scheduler.
  // Les taches tournent en USER/SYSTEM mode et le Scheduler tourne en Superviseur mode
	// Le processeur doit �tre en SUPERVISEUR quand vTaskStartScheduler est appel�
  // mais user privileged suffit en fait...	
	
	vTaskStartScheduler();

	/* Should never reach here!  If you do then there was not enough heap
	available for the idle task to be created. */
    LPC_GPIO3->FIOCLR = 1<<26;
		LPC_GPIO0->FIOCLR = 1<<22;
    LPC_GPIO3->FIOCLR = 1<<25;
	for( ;; )
	{ LPC_GPIO3->FIOSET = 1<<26;
		LPC_GPIO0->FIOCLR = 1<<22;
    LPC_GPIO3->FIOSET = 1<<25;
	}
}

void init_uart0(void)
{ LPC_SC->PCONP |= (1<<3)| (1<<4) ; // mise sous tension des peripheriques uart0 et uart1
	LPC_PINCON->PINSEL0 |= 0x00000050; // pattes P0.2 et P0.3 pour le module UART
  LPC_PINCON->PINMODE0 &= 0xFFFFFF0F; // on impose les pull UP sur les deux pattes
  LPC_UART0->LCR = 0x93 ;// DLAB /BREAK  PARITY forcee � 1  /PARITYENABLE  1bitSTOP  8bits
	 // la valeur est de  54.24 = 100 000 000/16/115200  
	LPC_UART0->DLM = 0; LPC_UART0->DLL = 54;//	 ainsi baudrate = 100 000/16/(0*256+54) = 9.615Kbauds
	LPC_UART0->FDR = 0x10; 
 	 // ou 	plus precis a tester : 12000/16/9.6/(1+D/M) qui amene 	  71.0227 plus precis si D=1 et M=10
	 //LPC_UART0->DLM = 0; U0DLL = 71;
	 //LPC_UART0->FDR = 0xA1;	// M D 

	 LPC_UART0->LCR = 0x13 ;	   // remettre DLAB = 0 pour acceder aux registres d'envoi et de r�ception 

	 LPC_UART0->ACR =0x00;  // auto baud control d�sactiv�
	 //LPC_UART0->ICR = 0x00;  // pas de liaison par infrarouge en IRDA (n existe pas pour U0)
   LPC_UART0->TER = 0x80; // autorise la transmission (on n'utilisera pas de controle de flux)
	 LPC_UART0->FCR = 0x07; // seuil 1ch  RST_TXfifo RST_RXfifo ENFIFO
	 LPC_UART0->IER = 0x00; // aucune interruption autoris�e
	/********************************************************************************/	
	// dans le main pour g�rer la liaison s�rie en r�ception et en envoi :
	 //on lira les caract�res recus dans U0RBR et on �crira dans U0THR
	 // les flag importants sont dans U0LSR

   // le Bit 0  pour savoir si on a re�u un caract�re : on l'utilise ainsi
	 // status_rs232 = LPC_UART0->LSR;
	 // if(status_rs232 & 1)
	 // {// on a recu un caract�re
	 //  if(status_rs232 & 0x0E) {/*il y a une erreur overrun ou parit� ou bit stop, on est PERDU */}
	 //  else {ch_recu=LPC_UART0->RBR; /* on va g�rer */} 

   // le bit5 pour savoir si on peut transmettre un caract�re 
	 // savoir qu'on peut en d�poser pendant qu'un autre est en cours de transmission
	 // le bit6 pour savoir si tous les caract�res sont finis de transmettre (registre � d�calage vide)
	 // status_rs232 = LPC_UART0->LSR;
   //if(status_rs232 & 1<<5) { LPC_UART0->THR= ch_a_envoyer;}
	 
  /***********************************************************************************/
}	

static void prvSetupHardware( void )
{
//   SCS|=1;  // port 0 et Port 1 en FastIO
 LPC_SC->PCONP     |= (1 << 15);            /* power on sur GPIO & IOCON , deja sous tension par d�faut*/ 

  LPC_GPIO3->FIODIR |= 0x03<<25;   //P3.25 et P3.26 en sortie
	LPC_GPIO0->FIODIR |= 0x01<<22;   //P0.22 en sortie	 
	init_uart0();
}

	
